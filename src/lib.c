#include <stdio.h>
#include <stdlib.h>
#include "linkedlist.h"
#include <string.h>

void initList(List *list_pointer)
{
	list_pointer->head = NULL;
	list_pointer->tail = NULL;
}

Node *createNode(void *item)
{
	Node newNode = {item, NULL};
	return &newNode;
}

void insertAtTail(List *list_pointer, void *item)
{
	//Create new Node from item and initialize
	Node *newNode = (Node *)malloc(sizeof(Node *));
	newNode->item = item;
	newNode->next = NULL; //Next is NULL because it will act as our new tail in the list, which refers to nothing as its the end
	list_pointer->tail->next = newNode;
	list_pointer->tail = newNode;
}

void insertAtHead(List *list_pointer, void *item)
{
	Node *newNode = (Node *)malloc(sizeof(Node *));
	newNode->item = item;
	newNode->next = list_pointer->head;
	if (list_pointer->head == NULL && list_pointer->tail == NULL)
	{
		list_pointer->tail = newNode;
	}
	list_pointer->head = newNode;
}

//Precondition: Assume index != 0 OR index != (index of tail), instead use insertAtHead() or insertAtTail()
void insertAtIndex(List *list_pointer, int index, void *item)
{
	Node *newNode = (Node *)malloc(sizeof(newNode));
	newNode->item = item;
	newNode->next = NULL;

	Node *temp = list_pointer->head;

	for (int i = 0; i < index - 1; i++)
	{
		temp = temp->next;
		if (temp == NULL || index <= 0)
		{ //Out of bounds check
			printf("You tried to insert outside the range of the list, use insertAtHead() or insertAtTail() instead.");
			return 1;
		}
	}
	newNode->next = temp->next;
	temp->next = newNode;
}

void *removeTail(List *list_pointer)
{
	Node* temp = list_pointer->head->next;
	Node* prev = list_pointer->head;
	Node* x = list_pointer->tail;

	while(temp->next != NULL){
		temp = temp->next;
		prev = prev->next;
	}
	
	list_pointer->tail = prev;
	prev->next = NULL;
	


	return x->item;
}

void *removeHead(List *list_pointer)
{	
	Node* temp = list_pointer->head;
	Node* newHead = list_pointer->head->next;
	list_pointer->head = newHead;
	
	return temp->item;
}

void *removeAtIndex(List *list_pointer, int index)
{	
	if(list_pointer->head == NULL){
		printf("The list is empty, you cannot remove any elements");
		return 1;
	}

	Node* temp = list_pointer->head;
	
	for(int i = 0; i < index-3; i++){
		temp = temp->next;
	}

	Node* oldNode = temp->next;
	temp->next = oldNode->next;
	oldNode->next = NULL;

	return oldNode->item;


}

void *itemAtIndex(List *list_pointer, int index)
{

	Node *temp = list_pointer->head;

	for (int i = 0; i < index; i++)
	{
		temp = temp->next;
		if (temp == NULL || index < 0)
		{ //Out of bounds check
			printf("You tried to insert outside the range of the list");
			return 1;
		}
	}

	return temp->item;
}

void printList(List *list)
{
	Node *node;

	// Handle an empty node. Just print a message.
	if (list->head == NULL)
	{
		printf("\nEmpty List\n");
		return;
	}

	// Start with the head.
	node = (Node *)list->head;

	printf("\nList: \n\n\t");
	while (node != NULL)
	{
		printf("[ %x ]", node->item);

		// Move to the next node
		node = (Node *)node->next;

		if (node != NULL)
		{
			printf("-->");
		}
	}
	printf("\n\n");
}
