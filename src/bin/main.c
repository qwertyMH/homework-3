#include "linkedlist.h"
#include <stdlib.h>

int test(char *label, int a, int b)
{
	printf("%s: ", label);
	if (a == b)
	{
		printf("Passed\n");
		return 1;
	}
	else
	{
		printf("Failed. lhs=%x, rhs= %x\n", a, b);
		exit(1);
	}
}

int main()
{
	// //Create List
	// List *list;
	// void *item;
	// list = (List *)malloc(sizeof(List));

	// //Initialize List
	// initList(list);
	// printList(list); //"Empty List"

	// //Build Nodes for list
	// int a = 1;
	// int b = 2;
	// int c = 3;
	// int d = 4;
	// int e = 5;
	// int f = 6;
	// int g = 7;

	// //Tests for inserts
	// insertAtHead(list, a); // 1
	// insertAtHead(list, b); // 2 1

	// insertAtTail(list, c); // 2 1 3
	// insertAtTail(list, d); // 2 1 3 4
	// insertAtTail(list, e); // 2 1 3 4 5

	// insertAtIndex(list, 3, f); // 2 1 3 6 4 5
	// insertAtIndex(list, 3, g); // 2 1 3 7 6 4 5

	// printList(list);

	// test("Test1", *(int *)itemAtIndex(list, 0), b);
	// test("Test2", *(int *)itemAtIndex(list, 5), d);
	// test("Test6", *(int *)removeTail(list), e);

	// printList(list);

	// test("Test7", *(int *)removeHead(list), b);

	// printList(list);

	// test("Test8", *(int *)removeAtIndex(list, 1), c);
	// printList(list);

	// test("Test8", *(int *)removeAtIndex(list, 1), g);
	// printList(list);

	//*************** Homework Tests****************
	List *lp;
	lp = (List *)malloc(sizeof(List));

	initList(lp);

	int v = 0x12ab34;
	int w = 0xfeed42;
	int x = 0xabcdef;
	int y = 0x123456;
	int z = 0xa1b2c3;
	insertAtHead(lp, &x);
	insertAtHead(lp, &y);
	insertAtTail(lp, &z);
	insertAtTail(lp, &v);
	insertAtIndex(lp, 1, &w);

	test("Test1", *(int *)itemAtIndex(lp, 0), 0x123456);
	test("Test2", *(int *)itemAtIndex(lp, 1), 0xfeed42);
	test("Test3", *(int *)itemAtIndex(lp, 2), 0xabcdef);
	test("Test4", *(int *)itemAtIndex(lp, 3), 0xa1b2c3);
	test("Test5", *(int *)itemAtIndex(lp, 4), 0x12ab34);
	test("Test6", *(int *)removeTail(lp), 0x12ab34);
	test("Test7", *(int *)removeHead(lp), 0x123456);
	test("Test8", *(int *)removeAtIndex(lp, 1), 0xabcdef);

	return 0;
}